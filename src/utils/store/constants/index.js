export const getAction = (type = '', arr) => {
  return arr.reduce((acc, item) => {
    acc[`${item}`] = `${type}/${item}`;
    return acc;
  }, {});
};
