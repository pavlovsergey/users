export const loadingAction = (action) => {
  return `${action}/loading`;
};

export const errAction = (action) => {
  return `${action}/fail`;
};

export const clearAction = (action) => {
  return `${action}/clear`;
};
export const setLoadingTrue = (state) => {
  return {
    ...state,
    isLoading: true,
    isLoad: false,
    isErrorLoad: false,
    error: { message: '', name: '' },
  };
};

export const setLoadTrue = (state) => {
  return {
    ...state,
    isLoading: false,
    isLoad: true,
  };
};

export const setLoadFail = (state, { message }) => {
  return {
    ...state,
    isLoading: false,
    isErrorLoad: true,
    error: { message },
  };
};

const defaultState = {
  isLoading: false,
  isLoad: false,
  isErrorLoad: false,
  error: { message: '', name: '' },
};

export const setDefaultState = () => defaultState;

export const defaultRerducerComunication = (action) => (
  state = defaultState,
  { type, payload }
) => {
  switch (type) {
    case loadingAction(action):
      return setLoadingTrue(state);

    case action:
      return setLoadTrue(state, payload);

    case errAction(action):
      return setLoadFail(state, payload);

    case clearAction(action):
      return setDefaultState();
    default:
      return state;
  }
};
