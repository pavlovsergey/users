import React from 'react';
import styles from './Tabs.module.scss';

const Tabs = ({ titles, onClick, indexActive }) => {
  return (
    <div className={styles.container}>
      {titles.map((title, index) => (
        <div
          key={index}
          onClick={() => onClick(index)}
          className={indexActive === index ? styles.active : styles.item}
        >
          {title}
        </div>
      ))}
    </div>
  );
};

export default Tabs;
