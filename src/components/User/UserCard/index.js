import React from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import styles from './UserCard.module.scss';

const UserCard = () => {
  const { idUser } = useParams();
  const userData = useSelector((state) => {
    return state.users.data.find(
      ({ id }) => `${id.name}${id.value}`.toLocaleLowerCase() === idUser
    );
  });

  if (!userData) return <div className={styles.container} />;
  return (
    <div className={styles.container}>
      <div className={styles.avatarContainer}>
        <img
          className={styles.avatar}
          src={userData.picture.large}
          alt='avatar'
        />
      </div>
      <div className={styles.content}>
        <div className={styles.string}>
          Address :
          <span className={styles.value}>
            {`${userData.location.country} ${userData.location.city} ${userData.location.street.name} ${userData.location.street.number}`}
          </span>
        </div>

        <div className={styles.string}>
          Email : <span className={styles.value}> {userData.email}</span>
        </div>

        <div className={styles.string}>
          Gender : <span className={styles.value}> {userData.gender}</span>
        </div>
        <div className={styles.string}>
          Date of birth :{' '}
          <span className={styles.value}> {userData.dob.date}</span>
        </div>
        <div className={styles.string}>
          Phone : <span className={styles.value}>{userData.phone}</span>
        </div>
      </div>
    </div>
  );
};

export default UserCard;
