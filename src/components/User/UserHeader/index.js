import React from 'react';
import { useHistory } from 'react-router-dom';
import styles from './UserHeader.module.scss';

const UserHeader = () => {
  const history = useHistory();
  return (
    <div className={styles.container}>
      <div onClick={() => history.push('/')} className={styles.btn}>
        Back
      </div>
    </div>
  );
};

export default UserHeader;
