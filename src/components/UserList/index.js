import React, { useEffect } from 'react';
import UserItem from './UserItem';
import Loader from 'components/Loaders/Loader';
import { getUsers } from 'store/users/actions';
import { usersDataSelector } from 'store/users/selectors';
import { useSelector, useDispatch } from 'react-redux';
import styles from './UserList.module.scss';

const UserList = ({ isFilter }) => {
  const dispatch = useDispatch();
  const { isLoading, isLoad, isErrorLoad } = useSelector(
    (state) => state.users.communication.get
  );
  let usersData = useSelector(usersDataSelector(isFilter));

  useEffect(() => {
    if (!isLoading && !isLoad && !isErrorLoad) dispatch(getUsers());
  }, [dispatch, isLoading, isLoad, isErrorLoad]);

  return (
    <div className={styles.container}>
      {isLoading ? <Loader title='Users loading' /> : null}
      <div className={styles.content}>
        {usersData.map((user, index) => (
          <UserItem
            key={user.id.value ? user.id.value + user.id.name : index}
            {...user}
          />
        ))}
      </div>

      {!isFilter && !isLoading ? (
        <div className={styles.btnContainer}>
          <div onClick={() => dispatch(getUsers())} className={styles.btn}>
            Load more
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default UserList;
