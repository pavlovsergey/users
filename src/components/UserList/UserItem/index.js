import React, { useState } from 'react';
import { selectUser, removeSelectedUser } from 'store/users/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import styles from './UserItem.module.scss';

const UserItem = ({ id, name, picture }) => {
  const [isDisubleHover, toggleHover] = useState(false);
  const idString = `${id.name} ${id.value}`;
  const dispatch = useDispatch();
  const selectedUser = useSelector((state) => state.users.selectedUser);
  const history = useHistory();

  const handleToUserPage = () => {
    history.push(`/${id.name}${id.value}`.toLocaleLowerCase());
  };

  const handleSelectUser = (e) => {
    e.stopPropagation();
    dispatch(selectUser(idString));
  };

  const handleRemoveSelectedUser = (e) => {
    e.stopPropagation();
    dispatch(removeSelectedUser(idString));
  };

  return (
    <div
      onClick={handleToUserPage}
      className={
        isDisubleHover
          ? styles.container
          : [styles.container, styles.activeContainer].join(' ')
      }
    >
      <div className={styles.avatarContainer}>
        <img className={styles.avatar} src={picture.medium} alt='avatar' />
      </div>
      <div className={styles.content}>
        <div className={styles.line}>
          id : <span className={styles.value}> {idString}</span>
        </div>
        <div className={styles.line}>
          Firstname : <span className={styles.value}> {name.first}</span>
        </div>
        <div className={styles.line}>
          Lastname : <span className={styles.value}>{name.last}</span>
        </div>
      </div>

      <div className={styles.btnContainer}>
        {selectedUser && selectedUser.includes(idString) ? (
          <div
            onMouseOver={() => toggleHover(true)}
            onMouseOut={() => toggleHover(false)}
            onClick={handleRemoveSelectedUser}
            className={styles.activeBtn}
          >
            cancel
          </div>
        ) : (
          <div
            onMouseOver={() => toggleHover(true)}
            onMouseOut={() => toggleHover(false)}
            onClick={handleSelectUser}
            className={styles.btn}
          >
            select
          </div>
        )}
      </div>
    </div>
  );
};

export default UserItem;
