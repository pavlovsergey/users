import React from 'react';
import loaderImage from './assets/loader.svg';
import styles from './Loader.module.scss';

const Loader = ({ title }) => {
  return (
    <div className={styles.loaderContainer}>
      <div className={styles.title}>{title} </div>
      <div className={styles.imgContainer}>
        <img src={loaderImage} alt='loading' />
      </div>
    </div>
  );
};

export default Loader;
