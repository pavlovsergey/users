import React, { useEffect, useState } from 'react';
import { setUserFilter } from 'store/users/actions';
import { useDispatch } from 'react-redux';
import styles from './Header.module.scss';

const Header = () => {
  const [value, setText] = useState('');
  const dispatch = useDispatch();
  useEffect(() => {
    Header.inputTimer && clearTimeout(Header.inputTimer);
    Header.inputTimer = setTimeout(() => {
      dispatch(setUserFilter(value.toLocaleLowerCase()));
    }, 250);

    return () => clearTimeout(Header.inputTimer);
  });

  return (
    <div className={styles.container}>
      <input
        value={value}
        className={styles.input}
        onChange={(e) => setText(e.target.value)}
      />
    </div>
  );
};

export default Header;
