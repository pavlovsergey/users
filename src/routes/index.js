import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Dashboard from 'pages/Dashboard';
import User from 'pages/User';
import Err from 'pages/Error';

const RouterApp = () => {
  return (
    <Switch>
      <Route exact path={`/error`} component={Err} />
      <Route path={`/:idUser`} component={User} />
      <Route path={`/`} component={Dashboard} />
    </Switch>
  );
};

export default RouterApp;
