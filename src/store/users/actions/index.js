import api from '../api';
import history from 'routes/history';
import actionTypes from '../constants';
import { plainErr } from 'store/error/actions';
import { loadingAction, errAction } from 'utils/store/communication';

export const getUsers = () => async (dispatch) => {
  dispatch({ type: loadingAction(actionTypes.get) });
  try {
    const { data } = await api.getUsers(20);
    data.results = data.results.map((item, index) => {
      if (!item.id.value) item.id.value = Date.now() + index;
      return item;
    });
    dispatch({
      type: actionTypes.get,
      payload: { data },
    });
  } catch (error) {
    dispatch(plainErr(error.toString()));
    history.push('/error');

    dispatch({
      type: errAction(actionTypes.get),
      payload: { message: error },
    });
  }
};

export const selectUser = (id) => {
  return { type: actionTypes.select, payload: { id } };
};

export const removeSelectedUser = (id) => {
  return { type: actionTypes.removeSelectedUser, payload: { id } };
};

export const setUserFilter = (value) => {
  return { type: actionTypes.setUserFilter, payload: { text: value } };
};
