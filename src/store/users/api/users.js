import axios from 'axios';

export const getUsers = (count) => {
  return axios.get(`https://randomuser.me/api/?results=${count}`, {
    headers: { Accept: 'json' },
  });
};
