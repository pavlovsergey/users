import { createSelector } from 'reselect';

export const usersDataSelector = (isFilter, state) =>
  createSelector(
    (state) => state.users.data,
    (state) => state.users.selectedUser,
    (state) => state.users.filter,
    (usersData, selectedUser, filter) => {
      if (filter) usersData = getUsersByFilter(usersData, filter);
      if (!isFilter) return usersData;

      return usersData.filter(({ id }) => {
        const idString = `${id.name} ${id.value}`;
        return selectedUser.includes(idString);
      });
    }
  );

const getUsersByFilter = (users, filter) => {
  return users.filter(({ name }) => {
    const nameString = `${name.first} ${name.last}`.toLocaleLowerCase();
    return filter.split(' ').every((word) => {
      return nameString.includes(word);
    });
  });
};
