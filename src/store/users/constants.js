import { getAction } from 'utils/store/constants';
export default getAction('users', [
  'get',
  'select',
  'removeSelectedUser',
  'setUserFilter',
]);
