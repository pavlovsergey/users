import actionTypes from 'store/users/constants';

const stateDefault = [];

const data = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case actionTypes.get: {
      if (!payload) return state;

      return [...state, ...payload.data.results];
    }

    default:
      return state;
  }
};

export default data;
