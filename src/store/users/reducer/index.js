import { combineReducers } from 'redux';
import data from './data';
import communication from './communication';
import selectedUser from './selectedUser';
import filter from './filter';

export default combineReducers({ data, communication, selectedUser, filter });
