import actionTypes from 'store/users/constants';

const stateDefault = [];

const selectedUser = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case actionTypes.select:
      return [...state, payload.id];

    case actionTypes.removeSelectedUser:
      return state.filter((item) => item !== payload.id);
    default:
      return state;
  }
};

export default selectedUser;
