import actionTypes from 'store/users/constants';

const stateDefault = '';

const filter = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case actionTypes.setUserFilter:
      return payload.text;

    default:
      return state;
  }
};

export default filter;
