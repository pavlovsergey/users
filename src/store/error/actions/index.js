import actionTypes from 'store/error/constants';

export const plainErr = (message) => {
  return { type: actionTypes.set, payload: { message } };
};

export const clearErr = () => {
  return { type: actionTypes.clear };
};
