import actionTypes from 'store/error/constants';

const stateDefault = '';

const message = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case actionTypes.set: {
      return payload.message.toString();
    }

    case actionTypes.clear: {
      return stateDefault ;
    }

    default:
      return state;
  }
};

export default message;
