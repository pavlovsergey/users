import { getAction } from 'utils/store/constants';
export default getAction('error', ['set', 'clear']);
