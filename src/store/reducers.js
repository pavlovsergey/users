import { combineReducers } from 'redux';
import users from './users/reducer';
import error from './error/reducer';

export default combineReducers({ users, error });
