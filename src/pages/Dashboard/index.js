import React, { useState } from 'react';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Tabs from 'components/Tabs';
import UserList from 'components/UserList';
import styles from './Dashboard.module.scss';

const Dashboard = () => {
  const [tab, setTab] = useState(0);

  return (
    <div className={styles.container}>
      <Header key={tab} />
      <Tabs
        indexActive={tab}
        onClick={setTab}
        titles={['All users', 'Choose users']}
      />
      <UserList isFilter={tab === 1} />}
      <Footer />
    </div>
  );
};

export default Dashboard;
