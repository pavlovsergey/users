import React, { useEffect } from 'react';
import { clearErr } from 'store/error/actions';
import { useSelector, useDispatch } from 'react-redux';
import history from 'routes/history';
import styles from './Error.module.scss';

const Err = () => {
  const dispatch = useDispatch();
  const error = useSelector((state) => state.error.message);
  useEffect(() => {
    if (!error) history.push('/');
    return () => dispatch(clearErr());
  }, [dispatch, error]);

  return <div className={styles.container}>{error}</div>;
};

export default Err;
