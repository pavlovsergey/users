import React from 'react';
import UserHeader from 'components/User/UserHeader';
import Footer from 'components/Footer';
import UserCard from 'components/User/UserCard';
import styles from './User.module.scss';

const User = () => {
  return (
    <div className={styles.container}>
      <UserHeader />
      <UserCard />
      <Footer />
    </div>
  );
};

export default User;
